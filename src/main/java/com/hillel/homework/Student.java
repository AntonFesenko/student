package com.hillel.homework;

import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Student {

    private String name;
    private int age;
    private List<Subject> subjects;
    private Teacher teacher;
    private Map<String, Object> mapSubjectMark;

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public void setMapSubjectMark(Map<String, Object> mapSubjectMark) {
        this.mapSubjectMark = mapSubjectMark;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void study() {
        System.out.println("My name is " + name + ", my age " + age
                + ". I study with teacher " + teacher.getName() + " these subjects "
                + subjects.stream().map(list -> list.getSubject()).collect(Collectors.joining(", "))
                + " and have these marks: "
                + mapSubjectMark.entrySet().stream().map(entry -> entry.getKey()
                + ": " + entry.getValue()).collect(Collectors.joining(", ")));
    }

    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:homework/app-field-ref.xml");
        ctx.refresh();
        Student student = ctx.getBean("student", Student.class);
        student.study();
        ctx.close();
    }

}
