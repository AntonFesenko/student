package com.hillel.homework;

public interface Subject {
    String getSubject();

    Integer getMark();
}
