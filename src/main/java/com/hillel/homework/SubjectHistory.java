package com.hillel.homework;

public class SubjectHistory implements Subject {
    private String name;
    private int mark;

    SubjectHistory() {
        this.name = "History";
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    @Override
    public String getSubject() {
        return name;
    }

    @Override
    public Integer getMark() {
        return mark;
    }

    @Override
    public String toString() {
        return name + " - " + " mark=" + mark;
    }
}
