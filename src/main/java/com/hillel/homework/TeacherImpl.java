package com.hillel.homework;

public class TeacherImpl implements Teacher {

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}
