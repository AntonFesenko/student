package com.hillel.homework.annotation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SubjectMath implements Subject {

    private String name;
    @Value("12")
    private int mark;

    SubjectMath() {
        this.name = "Math";
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    @Override
    public String getSubject() {
        return name;
    }

    @Override
    public Integer getMark() {
        return mark;
    }

    @Override
    public String toString() {
        return name + " - " + " mark=" + mark;
    }
}
