package com.hillel.homework.annotation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@ComponentScan(basePackages = "com.hillel.homework.annotation")
public class Student {

    @Value("Anton")
    private String name;
    @Value("38")
    private int age;
    @Autowired
    @Qualifier("teacherImpl")
    private Teacher teacher;

    @Autowired
    private List<Subject> subjects;
    @Autowired
    private Map<String, Subject> mapSubjectMark;


    public void study() {
        System.out.println("My name is " + name + ", my age " + age + ". I study with teacher "
                + teacher.getName() + " these subjects "
                + subjects.stream().map(list -> list.getSubject()).collect(Collectors.joining(", "))
                + " and have these marks: "
                + mapSubjectMark.entrySet().stream().map(entry -> entry.getKey() + ": "
                + entry.getValue()).collect(Collectors.joining(", ")));
    }

    public static void main(String[] args) {

        ApplicationContext ctx = new AnnotationConfigApplicationContext(Student.class);
        Student student = ctx.getBean(Student.class);
        student.study();
    }

}
