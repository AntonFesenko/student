package com.hillel.homework.annotation;

public interface Subject {
    String getSubject();

    Integer getMark();
}
