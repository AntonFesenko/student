package com.hillel.homework.annotation;

public interface Teacher {
    String getName();
}
